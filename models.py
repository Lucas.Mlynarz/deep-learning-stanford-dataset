import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

model_builder = {'resnet18'     : lambda:torchvision.models.resnet18(pretrained=True),
                 'resnet34'     : lambda:torchvision.models.resnet34(pretrained=True),
                 'resnet50'     : lambda:torchvision.models.resnet50(pretrained=True),
                 'resnet152'    : lambda:torchvision.models.resnet152(pretrained=True),
                 'densenet121'  : lambda:torchvision.models.densenet121(pretrained=True),
                 'squeezenet1_1': lambda:torchvision.models.squeezenet1_1(pretrained=True)}

imagenet_preprocessing = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                               std=[0.229, 0.224, 0.225])

preprocessings = {
        'resnet18'     : imagenet_preprocessing,
        'resnet34'     : imagenet_preprocessing,
        'resnet50'     : imagenet_preprocessing,
        'resnet152'    : imagenet_preprocessing,
        'densenet121'  : imagenet_preprocessing,
        'squeezenet1_1': imagenet_preprocessing,
        'mobilenetv2'  : imagenet_preprocessing
}

class Linear(nn.Module):
    def __init__(self, input_size, num_classes):
        super(Linear, self).__init__()
        self.layer_1 = nn.Linear(input_size, 256)
        self.layer_2 = nn.Linear(256, num_classes)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = x.view(x.size()[0], -1)
        x = self.relu(self.layer_1(x))
        x = self.layer_2(x)
        return x


class ConvAutoencoder(nn.Module):
    def __init__(self):
        super(ConvAutoencoder, self).__init__()
        self.conv1 = nn.Conv2d(in_channels = 3,
                               out_channels = 16,
                               kernel_size = 3,
                               padding = 1)
        self.conv2 = nn.Conv2d(in_channels = 16,
                               out_channels = 4,
                               kernel_size = 3,
                               padding = 1)
        self.t_conv1 = nn.ConvTranspose2d(in_channels = 4, out_channels = 16,
                                          kernel_size = 2, stride = 2)
        self.t_conv2 = nn.ConvTranspose2d(in_channels = 16, out_channels = 1,
                                          kernel_size = 2, stride = 2)
        self.relu = nn.ReLU()
        self.pool = nn.MaxPool2d(2, 2)
        self.sigmoid = nn.Sigmoid()
    
    def forward(self, x):
        #encode#                           
        x = self.relu(self.conv1(x))        
        x = self.pool(x)                  
        x = self.relu(self.conv2(x))      
        x = self.pool(x)                  
        #decode#
        x = self.relu(self.t_conv1(x))    
        x = self.sigmoid(self.t_conv2(x))
        return x