import torch
import torch.nn.init
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
import numpy as np
import random
import pathlib

from tqdm import tqdm

import os
import argparse

from classifier import Classifier
import loader
import models

CROP_SIZE = 256
NUM_LABELS = 14
NUMBER_EPOCHS = 3
DATADIR = pathlib.Path(loader.DATASET_DIR)
INPUTS_DIR = DATADIR / "data" / "rgb"
INPUTS = list(INPUTS_DIR.iterdir())
TARGETS_DIR = DATADIR / "data" / "semantic"
TARGETS = list(TARGETS_DIR.iterdir())


parser = argparse.ArgumentParser()
parser.add_argument("mode", type=str, help="train or test")

TRANSFORM = [transforms.Resize((256, 256)),
        transforms.CenterCrop(CROP_SIZE),
        transforms.Resize((256, 256)),
        transforms.ToTensor()]

if __name__ == "__main__":
    args = parser.parse_args()
    mode = args.mode
    model_type = "linear"

    device = (torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu"))

    if mode == "train":
        MyClassifier = Classifier(
            inputs_dir= INPUTS,
            targets_dir = TARGETS,
            num_labels=NUM_LABELS,
            device=device,
            Transform=TRANSFORM,
        )

        train_loader, val_loader = MyClassifier.load_data()

        MyClassifier.load_model(model_type=model_type)

        MyClassifier.fit(num_epochs=NUMBER_EPOCHS,train_loader=train_loader)
