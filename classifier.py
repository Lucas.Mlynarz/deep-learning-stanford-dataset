import loader
import torch
import torch.nn as nn
import models
from tqdm import tqdm
import os
from torch.utils.tensorboard import SummaryWriter

DATASET_DIR = "/mounts/Datasets4/Stanford2D-3D-S/area_3/"
VAL_PROP = 0.1
NUMBER_EPOCHS = 3
TSBOARD_PATH = "../tensorboards/"
LR = .01
BATCH_SIZE = 16

class Classifier:
    def __init__(
        self,
        inputs_dir: str,
        targets_dir: str,
        num_labels: int,
        device,
        Transform=None,
        learn_rate = LR,
    ):
        self.inputs_dir = inputs_dir
        self.targets_dir = targets_dir
        self.num_labels = num_labels
        self.device = device
        self.batch_size = BATCH_SIZE
        self.Transform = Transform
        self.learn_rate = learn_rate
        self.name = ""


    def load_data(self):
        print("loading data")
        dataset = loader.StanfordDataset(self.inputs_dir, self.targets_dir, transform=self.Transform)
        val_size = int(len(dataset) * VAL_PROP)
        train_size = len(dataset) - val_size
        trainset, valset = torch.utils.data.random_split(dataset, [train_size, val_size])

        train_loader = torch.utils.data.DataLoader(trainset, batch_size=self.batch_size, pin_memory=True)
        val_loader = torch.utils.data.DataLoader(valset, batch_size=self.batch_size, pin_memory=True)

        return train_loader, val_loader

    def load_model(self, model_type="linear"):
        print("loading model")
        self.name = model_type
        self.model = models.ConvAutoencoder()
        self.model = self.model.to(self.device)
        self.optimizer = torch.optim.Adam(self.model.parameters(), self.learn_rate)
        self.criterion = nn.MSELoss()
    

    def fit_one_epoch(self, train_loader, epoch, num_epochs):
        train_losses = []
        train_acc = []

        self.model.train()
        for i, (images, targets) in enumerate(tqdm(train_loader)):
            images = images.to(self.device)
            targets = targets.to(self.device).squeeze()
            preds = self.model(images).squeeze()
            loss = self.criterion(preds, targets)
            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            train_losses.append(loss.item())

        train_loss = torch.tensor(train_losses).mean()
        print(f"Epoch n° {epoch}/{num_epochs-1}")
        print(f"Training Loss: {train_loss:.2f}")
        return train_loss
    

    def fit(self, train_loader, num_epochs=NUMBER_EPOCHS):

        if not os.path.exists(TSBOARD_PATH):
            os.mkdir(TSBOARD_PATH)

        tsboardpath = TSBOARD_PATH + self.name + str(0)
        os.mkdir(tsboardpath)

        TensorboardWriter = SummaryWriter(log_dir=tsboardpath)

        for epoch in range(num_epochs):
            train_loss = self.fit_one_epoch(train_loader, epoch, num_epochs)            
            TensorboardWriter.add_scalar("train loss", float(train_loss), epoch)
