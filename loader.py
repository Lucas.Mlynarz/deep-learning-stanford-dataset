from torchvision import transforms
import torchvision.transforms.functional as TF
import torch
import torchvision
import numpy as np
from torch.utils.data import Dataset, random_split
import pathlib
import PIL.Image as Image
import json

DATASET_DIR = "/mounts/Datasets4/Stanford2D-3D-S/area_3/"

def get_index(color):
    return color[0] * 256 * 256 + color[1] * 256 + color[2]

def get_color(i):
    b = (i) % 256  # least significant byte
    g = (i >> 8) % 256
    r = (i >> 16) % 256  # most significant byte
    return r, g, b

def load_semantic(filename, lbl_map):
    # Load the RBG image 24 bit base 256 encoded
    semantic_img = np.array(Image.open(filename))
    # Convert the RGB code into the index code

    semantic = np.zeros(semantic_img.shape[:2], dtype="int")
    for i in range(semantic_img.shape[0]):
        for j in range(semantic_img.shape[1]):
            semantic[i, j] = lbl_map[get_index(semantic_img[i, j, :])]
    return semantic

device = (torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu"))

datadir = pathlib.Path(DATASET_DIR)
rgbdir = datadir / "data" / "rgb"
semanticdir = datadir / "data" / "semantic"

#rgbfiles = list(rgbdir.iterdir())
rgbfiles = list(rgbdir.glob("*.png"))
#semanticfiles = list(semanticdir.iterdir())
semanticfiles = list(semanticdir.glob("*.png"))


with open("semantic_labels.json") as f:
        json_labels = json.load(f)

labels = sorted(list(set([lblname.split("_")[0] for lblname in json_labels])))
lbl_map = {ik: labels.index(k.split("_")[0]) for ik, k in enumerate(json_labels)}
lbl_map[int(0x0D0D0D)] = labels.index("<UNK>")  


class StanfordDataset(Dataset):

        def __init__(self,inputs,targets,
                transform=None,
                lbl_map = lbl_map, labels = labels):

                self.inputs = inputs
                self.targets = targets
                self.transform = transform
                self.lbl_map = lbl_map
                self.labels = labels
        
        def __len__(self):
                return len(self.inputs)

        def __getitem__(self,idx):
                image = np.array(Image.open(self.inputs[idx]))
                semantic = load_semantic(self.targets[idx], self.lbl_map)
                image = torch.tensor(image, dtype = torch.float32)
                semantic = torch.tensor(semantic, dtype = torch.float32)
                image = torch.reshape(image,(3,1080,1080))
                semantic = torch.reshape(semantic,(1,1080,1080))
                #image = torchvision.transforms.CenterCrop((256,256))(image)
                #semantic = torchvision.transforms.CenterCrop((256,256))(semantic)
                return image, semantic

dataset = StanfordDataset(rgbfiles,semanticfiles)
im, sem = dataset[0]
print(sem.size())
print(im.size())
